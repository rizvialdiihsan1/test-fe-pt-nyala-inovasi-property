import React, { useState } from "react";

const TestFrontEnd = () => {
  const [hasil, setHasil] = useState(null);
  const [str, setStr] = useState("");
  const [loading, setLoading] = useState(true);

  const handleChange = (e) => {
    setStr(e.target.value);
  };

  const palindromeFunction = () => {
    let value = str.replace(/[.,?:;\/() _-]/g, "").toLowerCase();
    let result = false;
    for (var i = 0; i < Math.floor(value.length / 2); i++) {
      if (value[i] === value[value.length - 1 - i]) {
        result = true;
      } else {
        result = false;
      }
    }
    setHasil(result);
    setLoading(false);
  };

  return (
    <div style={{marginBottom: '5%', color: 'green'}}>
      <h4>
        <u>1. Test Frontend Development Palindrome</u>
      </h4>
      <input type="text" onChange={handleChange}></input>
      <button onClick={palindromeFunction}>check Palindrome </button>
      {!loading && (
        <h5>{hasil ? "ini adalah palindrome" : "ini bukan palindrome   "}</h5>
      )}
    </div>
  );
};

export default TestFrontEnd;
