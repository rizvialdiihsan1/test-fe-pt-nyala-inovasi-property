import React, { useState } from "react";

const TestFrontEnd2 = () => {
  const [hasil, setHasil] = useState(null);
  const [amountLeft, setAmountLeft] = useState(null);
  const [money, setMoney] = useState(null);
  const [process, setProcess] = useState(true);

  const handleChange = (e) => {
    setHasil(null);
    setAmountLeft(null);
    let value = e.target.value.replace(/[^0-9\,]/g, "");
    setMoney(parseFloat(value));
  };

  const processMoney = () => {
    setHasil(null);
    setAmountLeft(null);
    const fractions = [
      100000,
      50000,
      20000,
      10000,
      5000,
      2000,
      1000,
      500,
      200,
      100,
    ];
    let amountMoney = money;
    let temp = [];
    let result = [];
    const validation = validationAmount(money);
    while (amountMoney >= 100) {
      for (var i = 0; i < fractions.length; i++) {
        if (amountMoney >= fractions[i]) {
          temp[fractions[i]] = temp[fractions[i]] ? temp[fractions[i]] + 1 : 1;
          amountMoney = amountMoney - fractions[i];
          break;
        }
      }
    }
    let resultOrder = temp.length - 1;
    temp.forEach((value, key) => {
      result[resultOrder] = { money: key, total: value };
      resultOrder--;
    });
    setAmountLeft(amountMoney);
    setHasil(result);
    setProcess(false);
  };

  const validationAmount = (amount) => {
    if (amount < 100 || amount > 100000000) {
      return alert("error input value is invalid");
    }
  };

  return (
    <div style={{ marginBottom: "5%", color: "green" }}>
      <h4>
        <u>2. Test Frontend Development Calculate Money Fraction</u>
      </h4>
      <input
        type="text"
        pattern="[0-9]"
        onChange={(e) => handleChange(e)}
      ></input>
      <button onClick={processMoney}>check Fraction </button>
      <br />
      <h5>
        {!process && (
          <div>
            {money && <span>{money} = </span>}
            {hasil &&
              hasil?.map((val, i) => {
                return (
                  <span key={i}>
                    {val.money}({val.total}X) ,
                  </span>
                );
              })}
            {amountLeft && <span>Left = {amountLeft} </span>}
          </div>
        )}
      </h5>
    </div>
  );
};

export default TestFrontEnd2;
