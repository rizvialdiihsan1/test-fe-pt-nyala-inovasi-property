import logo from "./logo.svg";
import "./App.css";
import TestFrontEnd1 from "./component/test-3";
import TestFrontEnd2 from "./component/test-3-2";

function App() {
  return (
    <div className="App">
      <h1> Test Frontend Development PT Nyala Inovasi Property</h1>
      <div style={{border: 'solid',marginTop: '5%'}}>
      <TestFrontEnd1 />
      </div>
      <div style={{border: 'solid', marginBottom: "5%"}}>
      <TestFrontEnd2 />
      </div>
    </div>
  );
}

export default App;
